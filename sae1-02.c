#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "sae1-02.h"

// Partie 1


// PARTIE ENREGISTREMENT
Listdept initialisation(void) {
    return NULL;
}

VilleIUT lireVille(FILE *fe) {
		VilleIUT v;
		fscanf(fe, "%s %d", v.Ville, &v.nbIUT);
		return v;
}

Listdept lireDept(FILE *fe, int nbIUT) {
	if(nbIUT == 0) return NULL;
	MaillonDept* d;
	d = (MaillonDept*)malloc(sizeof(MaillonDept));
	fscanf(fe, "%s %d", d->departement, &d->nbplace);
	fgets(d->responsable, 31, fe);
	d->responsable[strlen(d->responsable)-1] = '\0';
	d->suivant = lireDept(fe, nbIUT - 1);
	return d;
}

int chargeIUT(VilleIUT* tIUT[], int taillemax) {
	FILE * fe = fopen("donneesIUT.don", "r");
	int i = 0;
	VilleIUT v;
	Listdept d = initialisation();

	if(fe == NULL) { printf("Erreur ouverture fichier \n"); return -1; }

	v = lireVille(fe);
	while(!feof(fe)) {
		if (i == taillemax) { printf("erreur taille tableau \n"); return -1; }
		tIUT[i] = (VilleIUT*) malloc(sizeof(VilleIUT));
		if(tIUT[i] == NULL) { printf("erreur tab\n"); return -1; }
		d = lireDept(fe, v.nbIUT);
		v.ldept = d;
		*tIUT[i] = v;

		v = lireVille(fe);
		i++;
	}
	fclose(fe);
	return i;
}

// PARTIE UTILISATEUR //////////

// villes contenant un IUT
void afficheVilleIUT(VilleIUT *tville[], int nbville) {
	printf("Villes contenant un IUT : \n");
	for(int i = 0; i < nbville; i++) {
		printf(" - %s\n", tville[i]->Ville);
	}
}

// départements de chaques IUT
void afficheDepartementsIUT(VilleIUT *tville[], int nbville) {
	Listdept d;

	int i = rechercheVille(tville, nbville);
	if (i == -1) return;// cas où rien est trouvé en parcourant le tableau

	d = tville[i]->ldept;
	printf("Département(s) : \n");
	affichageDep(d);
}

// nombre de place 
void afficheDepNbPlace(VilleIUT *tville[], int nbville) {
	printf("Voici le nombre de place pour chaques départements de chaques IUT : \n");
	affichageville(tville, nbville);
}

// IUT avec un département particulier
void afficheIUTdepParticulier(VilleIUT *tville[], int nbville) {
	char rechercheDep[31];
	Listdept d;
	int cpt = 0;
	printf("Quelle département recherchez vous en particulier ? : ");
	scanf("%s", rechercheDep);

	printf("Voici les villes avec le département %s : \n", rechercheDep);
	for (int i = 0; i < nbville; i++) {
		d = tville[i]->ldept;
		if(strcmp(rechercheDep, d->departement) == 0) {
			printf(" - %s\n", tville[i]->Ville);
			cpt++;
		}
	}
	if(cpt == 0) printf("Aucun IUT avec ce département n'a été trouvé\n");
}

// PARTIE ADMINISTRATEUR /////////

// modif nombre place département
void modifPlace(VilleIUT *tville[], int nbville) {
	Listdept d;
	char rechercheDep[31];
	int i = rechercheVille(tville, nbville);
	if (i == -1) return;// cas où rien est trouvé en parcourant le tableau
	d = tville[i]->ldept;
	printf("Et quel département pour %s ? : ", tville[i]->Ville);
	scanf("%s", rechercheDep);
	int cpt = chercheDepModif(d, rechercheDep);
	if(cpt == 0) printf("Aucun département de ce nom\n");
}

int chercheDepModif(Listdept d, char rechercheDep[30]) {
	int cpt = 0;
	if (d == NULL) return cpt;
	if(strcmp(d->departement, rechercheDep) == 0) {
		printf("Nombre de place actuellement : %d\nNouveau nombre de place : ", d->nbplace);
		scanf("%d", &d->nbplace);
		cpt++;
	}
	else return chercheDepModif(d->suivant, rechercheDep);
}

// recherche ville
int rechercheVille(VilleIUT *tville[], int nbville) {
	char recherche[31];
	printf("Quelle IUT recherchez vous ? : ");
	scanf("%s", recherche);
	for(int i = 0; i < nbville; i++) {
		if(strcmp(recherche, tville[i]->Ville) == 0) return i;
	}
	printf("L'IUT n'a pas été trouvé !\n");
	return -1;
}


// ajouter un département IUT
Listdept ajoutDept(int posOptionnel, VilleIUT *tville[], int nbville) {
	int i;
	MaillonDept* m;
	Listdept d;
	m = (MaillonDept*)malloc(sizeof(MaillonDept));
	if (m == NULL) { printf("Problème malloc"); exit(1);}

	if(posOptionnel == -1) i = rechercheVille(tville, nbville); // Demande dans quelle IUT on souhaite ajouter le département
	else i = posOptionnel;
	if (i == -1) return m;
	
	d = tville[i]->ldept;

	printf("Veuillez entrer le nom du nouveau département : ");
	scanf("%s", m->departement);
	printf("Entrez le nombre de place pour le département %s de %s : ", m->departement, tville[i]->Ville);
	scanf("%d ", &m->nbplace);
	printf("Entrez le nom du responsable : ");
	fgets(m->responsable, 31, stdin);
	m->responsable[strlen(m->responsable)-1]='\0';
	m->suivant = d;
	tville[i]->nbIUT++; // augmente le nombre d'IUT enregistré
	if(posOptionnel == -1) tville[i]->ldept = m;
	return m;
}

// ajouter un IUT 
void ajoutIUT(VilleIUT *tIUT[], int *nbville, int taillemax) {
	Listdept d;

	if (*nbville == taillemax) { printf("erreur taille tableau \n"); return; }
	tIUT[*nbville] = (VilleIUT*) malloc(sizeof(VilleIUT));
	if(tIUT[*nbville] == NULL) { printf("erreur tab\n"); return; }

	printf("Entrez le nom de la ville à ajouter: ");
	scanf("%s", tIUT[*nbville]->Ville);
	tIUT[*nbville]->nbIUT = 0;

	d = ajoutDept(*nbville, tIUT, *nbville);
	tIUT[*nbville]->ldept = d;
	*nbville = *nbville + 1;
}

// supprimer un département
Listdept suppressionDept(Listdept d, char rechercheDep[31]){
	Listdept tmp;
	if(d == NULL){ exit(1); }
	if(strcmp(d->departement, rechercheDep) == 0){
		tmp = d->suivant;
		free(d);
		return tmp;
	}
	d = suppressionDept(d->suivant, rechercheDep);
}

void supprDepartement(VilleIUT *tville[], int *nbville) {
	int i = rechercheVille(tville, *nbville);
	if (i == -1) return;
	Listdept d = tville[i]->ldept;


	char rech[31];
	printf("Quel département à supprimer pour la ville de %s : ", tville[i]->Ville);
	scanf("%s", rech);

	tville[i]->ldept = suppressionDept(d, rech);
	tville[i]->nbIUT = tville[i]->nbIUT - 1;

	if (tville[i]->nbIUT == 0) {
		for (i; i < *nbville - 1; i++) {
			strcpy(tville[i]->Ville, tville[i + 1]->Ville);
			tville[i]->nbIUT = tville[i + 1]->nbIUT;
			tville[i]->ldept = tville[i + 1]->ldept;
		}
		*nbville = *nbville - 1;
	}
}


void modifNomResponsable(VilleIUT *tville[], int nbville) {
	int i = rechercheVille(tville, nbville);
	if (i == -1) return;
	Listdept d = tville[i]->ldept;

	char rech[31];
	printf("Modifier le nom du responsable de quel département pour %s ? : ", tville[i]->Ville);
	scanf("%s", rech);

// bug a corriger
	fgets(d->responsable, 31, stdin);
	d->responsable[strlen(d->responsable)-1]='\0';

	fgets(d->responsable, 31, stdin);
	d->responsable[strlen(d->responsable)-1]='\0';

	tville[i]->ldept = d;
}

// PARTIE AFFICHAGE

void affichageDep(Listdept d) {
	if(d == NULL) { return; }
	printf(" - %s\n", d->departement);
	affichageDep(d->suivant);
}

void affichageAllDep(Listdept d){
	if(d == NULL) { return; }
	printf("Département : %s\nNombre place : %d\nResponsable : %s\n\n", d->departement, d->nbplace, d->responsable);
	affichageAllDep(d->suivant);
}

void affichageville(VilleIUT *tville[], int nbville){
	int i;
	Listdept d;
	for(i = 0; i < nbville; i++) {
		d = tville[i]->ldept;
		printf("%s -\n", tville[i]->Ville);
		affichageAllDep(d);
		printf("\n");
	}
}

void sauvegardeDep(Listdept d, FILE *fs) {
	if(d == NULL) return;
	fprintf(fs, "%s\t%d%s\n\n", d->departement, d->nbplace, d->responsable);
	sauvegardeDep(d->suivant, fs);
}

void sauvegarde(VilleIUT *tville[], int nbville) {
	FILE *fs;
    fs = fopen ("donneesIUT.don", "w");
    if (fs == NULL) { printf ("Erreur, ouverture fichier"); return; }

    for (int i = 0; i < nbville; i++) {
    	fprintf(fs, "%s\t%d\n", tville[i]->Ville, tville[i]->nbIUT);
    	sauvegardeDep(tville[i]->ldept, fs);
    }
    fclose(fs);
}

// Partie 2

//chargement ======================================================================================================================================================================

Listchoixdept listvide(void){return NULL;}

Listchoixdept lireCarte(FILE *fe, int nbchoix){
	if(nbchoix==0){return NULL;}
	Maillonchoix* cc;
	cc = (Maillonchoix*)malloc(sizeof(Maillonchoix));
	fscanf(fe, "%s %s %d %d", cc->Ville, cc->dep, &cc->decision, &cc->validation);
	cc->suivant = lireCarte(fe, nbchoix-1);
	return cc;
}

listetuinfo lireEtu(FILE *fe){
 	listetuinfo b;
	int i;
	fscanf(fe, "%d %s %s %d %d %d %d %d", &b.numeroetu, b.nometu, b.prenometu, &b.notes[0], &b.notes[1], &b.notes[2], &b.notes[3], &b.nbchoix);
	return b;
}

int chargeretudiant(char nomFich[], listetuinfo *tetu[], int tmax){
	int i=0, nbetu;
	listetuinfo a;
	Listchoixdept b;
	FILE *fe;
	fe = fopen( nomFich, "r");
	if(fe==NULL){printf("<! pb ouverture fichier in chargeretudiant !>\n");return-1;}
	fscanf(fe, "%d", &nbetu);
	b = listvide();
	for(i=0; i<nbetu; i++){
		if(i==tmax){printf("<! erreur fichier in chargeetudiant !>");fclose(fe);return-1;}
		tetu[i] = (listetuinfo*)malloc(sizeof(listetuinfo));
		if(tetu[i]==NULL){printf("<! pb malloc in chargeretudiant !>\n");fclose(fe);return-1;}
		a = lireEtu(fe);
		b = lireCarte(fe, a.nbchoix);
		a.carte = b;
		*tetu[i] = a;
	}
	fclose(fe);
	return nbetu;
}

//rechercher etu ==============================================================================================================================================================

int recherdicoetu(listetuinfo *tetu[], int nb, int num){
	int d=0, m, f=nb-1;
	while(d<=f){
		m=(d+f)/2;
		if(tetu[m]->numeroetu==num){return m;}
		if(tetu[m]->numeroetu>num){
			f=m-1;
		}else{
			d=m+1;
		}
	}
	return -1;
}

//enregistrement ========================================================================================================================================================

void enregistrementcarte(FILE *fe, Maillonchoix* carte, int nb){
	if(carte==NULL){return;}
	fprintf(fe, "%s\n%s\n%d\n%d\n", carte->Ville, carte->dep, carte->decision, carte->validation);
	enregistrementcarte(fe, carte->suivant, nb-1);
	free(carte);
}

void enregistrementinfo(FILE *fe, listetuinfo *etu){
	Listchoixdept b;
	int i=0;
	b = etu->carte;
	fprintf(fe, "%d\n%s\n%s\n%d %d %d %d\n%d\n", etu->numeroetu, etu->nometu, etu->prenometu, etu->notes[0], etu->notes[1], etu->notes[2], etu->notes[3], etu->nbchoix);
	enregistrementcarte(fe, b, etu->nbchoix);
}

int enregistrementetudiants(char nomFich[], listetuinfo *tetu[], int nb){
	int i=0;
	FILE *fe;
	listetuinfo *etu;
	fe = fopen( nomFich, "w");
	if(fe==NULL){printf("<! pb fichier in enregistrementetudiant !>\n");return-1;}
	fprintf(fe, "%d\n", nb);
	for(i=0; i<nb; i++){
		etu = tetu[i];
		enregistrementinfo(fe, etu);
		free(tetu[i]);
	}
	fclose(fe);
}

//=============================================================================================================================================================================================================================

//affichage =======================================================================================================================================================

//affiche que les cartes d'une personne
void affichagecarte(Listchoixdept c){
	if(c==NULL){printf("\n");return;}
	printf(" - %s - %s\n \tdécision : %d\n \tvalidation : %d", c->Ville, c->dep, c->decision, c->validation);
	if(c->suivant==NULL){affichagecarte(c->suivant);}
	else{
		printf("\n");
		affichagecarte(c->suivant);
	}
}

//affiche que la personne
void affichageetu(listetuinfo a){
	printf(" %d\n %s %s\n", a.numeroetu, a.nometu, a.prenometu);
	printf(" mathématique : %d -", a.notes[0]);
	printf(" français : %d -", a.notes[1]);
	printf(" anglais : %d -", a.notes[2]);
	printf(" matière spé : %d\n", a.notes[3]);
}

//affichage general
void affichealletu(listetuinfo *tetu[], int nb){
	int j=0;
	Listchoixdept b;
	b = listvide();
	for(j=0; j<nb; j++){
		affichageetu(*tetu[j]);
		printf("tout les choix :\n - ");
		b = tetu[j]->carte;
		affichagecarte(b);
		printf("\n\n");
	}
}

//général fonction for ajouter modifier et supprimer ========================================================================================

int cherchelistcorrespond(MaillonDept* dep, char choixdep[]){
	if(dep==NULL){return -1;}
	if(strcmp(dep->departement,choixdep)==0){return 1;}
	return cherchelistcorrespond(dep->suivant, choixdep);
}


int correspondville(char choixville[], char choixdep[], VilleIUT *tville[], int nbville){
	int i, ok;
	Listdept b;
	for(i=0; i<nbville; i++){
		if(strcmp(tville[i]->Ville,choixville)==0){
			b = tville[i]->ldept;
			ok = cherchelistcorrespond(b, choixdep);
			if(ok==1){return 1;}
		}
	}
	return -1;
}

int correspondcarte(char choixville[], char choixdep[], Listchoixdept carte){
	if(carte==NULL){return 0;}
	if(strcmp(choixville,carte->Ville)==0){
		if(strcmp(choixdep,carte->dep)==0){
			return 1;
		}
	}
	correspondcarte(choixville, choixdep, carte->suivant);
}



//ajouter =============================================================================================================================================

void choix(VilleIUT *tville[], int nbville, char choixville[], char choixdep[]){
	int i, ok=1;
	printf("ville : ");
	fgets(choixville, 31, stdin);
	fgets(choixville, 31, stdin);
	choixville[strlen(choixville)-1]='\0';
	printf("departement : ");
	fgets(choixdep, 31, stdin);
	choixdep[strlen(choixdep)-1]='\0';
	ok = correspondville(choixville, choixdep, tville, nbville);
	while(ok!=1){
		printf("ville : \n");
		fgets(choixville, 31, stdin);
		choixville[strlen(choixville)-1]='\0';
		printf("departement : \n");
		fgets(choixdep, 31, stdin);
		choixdep[strlen(choixdep)-1]='\0';
		ok = correspondville(choixville, choixdep, tville, nbville);
	}
}

//ajouter en tete :)
Listchoixdept ajoutercarte(Maillonchoix* a, char choixville[], char choixdep[]){
	Maillonchoix* carte;
	carte = (Maillonchoix*)malloc(sizeof(Maillonchoix));
	if(carte==NULL){printf("<! erreur malloc in ajoutercarte !>\n");exit(1);}
	strcpy(carte->Ville,choixville);
	strcpy(carte->dep,choixdep);
	carte->decision=0;
	carte->validation=0;
	carte->suivant=a;
	return carte;
}

listetuinfo allajoutprocess(VilleIUT *tville[], listetuinfo etu, int nbville){
	int i, ok;
	Listchoixdept b;
	b = etu.carte;
	char choixville[31], choixdep[31];
	affichageville(tville, nbville);
	choix(tville, nbville, choixville, choixdep);
	printf("%s %s \n", choixville, choixdep);
	ok = correspondcarte(choixville, choixdep, b);
	printf("%d\n", ok);
	if(ok==0){b = ajoutercarte(b, choixville, choixdep);}
	else{printf("\t< choix deja existant >\n");}
	etu.carte=b;
	return etu;
}

//modifier ==============================================================================================================================================================

void choix2(Listchoixdept lcarte, char choixville[], char choixdep[]){
	int ok;
	printf("ville : \n");
	fgets(choixville, 31, stdin);
	fgets(choixville, 31, stdin);
	choixville[strlen(choixville)-1]='\0';
	printf("departement : \n");
	fgets(choixdep, 31, stdin);
	choixdep[strlen(choixdep)-1]='\0';
	ok = correspondcarte(choixville, choixdep, lcarte);
	while(ok!=1){
		printf("ville : \n");
		fgets(choixville, 31, stdin);
		choixville[strlen(choixville)-1]='\0';
		printf("departement : \n");
		fgets(choixdep, 31, stdin);
		choixdep[strlen(choixdep)-1]='\0';
		ok = correspondcarte(choixville, choixdep, lcarte);
	}
}

void fchoixdep(char choixville[], char choixdep[], VilleIUT *tville[], int nbville){
	int ok;
	printf("\n< quel departement voulez vous choisir ? >\n==>");
	fgets(choixdep, 31, stdin);
	choixdep[strlen(choixdep)-1]='\0';
	ok = correspondville(choixville, choixdep, tville, nbville);
	while(ok!=1){
		printf("\n< quel departement voulez vous choisir ? >\n==>");
		fgets(choixdep, 31, stdin);
		choixdep[strlen(choixdep)-1]='\0';
		ok = correspondville(choixville, choixdep, tville, nbville);
	}
}

Listchoixdept choixmodifcarte(Listchoixdept dep, char choixville[], char choixdep[], VilleIUT *tville[], int nbville){
	if(dep==NULL){exit(1);}
	if(strcmp(dep->Ville,choixville)==0){
		if(strcmp(dep->dep,choixdep)==0){
			fchoixdep(choixville, choixdep, tville, nbville);
			strcpy(dep->dep,choixdep);
			return dep;
		}
	}
	dep->suivant = choixmodifcarte(dep->suivant, choixville, choixdep, tville, nbville);
	return dep;
}

void affichagedep(MaillonDept* carte){
	if(carte==NULL){return;}
	printf("%s", carte->departement);
	affichagedep(carte->suivant);
}

void affichervillechoix(VilleIUT *villechoix){
	int i;
	Listdept ldep;
	ldep = villechoix->ldept;
	printf("%s :\n", villechoix->Ville);
	affichagedep(ldep);
	printf("\n");
}

int correspondvilleonly(char choixville[], VilleIUT *ville[], int nbville){
	int i;
	for(i=0; i<nbville; i++){
		if(strcmp(ville[i]->Ville,choixville)==0){
			return 1;
		}
	}
}

void modificationcarte(listetuinfo etu, VilleIUT *tville[], int nbville){
	int ok;
	char choixville[31], choixdep[31];
	Listchoixdept lcarte;
	lcarte=etu.carte;
	affichagecarte(lcarte);
	choix2(lcarte, choixville, choixdep);
	ok = correspondvilleonly(choixville, tville, nbville);
	if(ok==1){
		lcarte = choixmodifcarte(lcarte, choixville, choixdep, tville, nbville);
		etu.carte=lcarte;
	}
	else{printf("\t< choix pas existant >\n");}
}

//supprimer ==========================================================================================================================================

Maillonchoix* supp(Listchoixdept carte){
	Maillonchoix *tmp;
	tmp=carte->suivant;
	free(carte);
	return tmp;
}

Maillonchoix* suppressioncartechoix(char choixville[], char choixdep[], Listchoixdept carte){
	if(carte==NULL){exit(1);}
	if(strcmp(carte->Ville,choixville)==0){
		if(strcmp(choixdep,carte->dep)==0){
			carte = supp(carte);
			return carte;
		}
	}
	carte->suivant = suppressioncartechoix(choixville, choixdep, carte->suivant);
	return carte;
}

void suppcarte(listetuinfo etu){
	int i, ok;
	char choixville[31], choixdep[31];
	Listchoixdept lcarte;
	lcarte = etu.carte;
	affichagecarte(lcarte);
	choix2(lcarte, choixville, choixdep);
	lcarte = suppressioncartechoix(choixville, choixdep, lcarte);
	etu.carte=lcarte;
}

//==========================================================================================================

// Partie 3



DepartementVille* recupererEtudiantsDepartementVille(listetuinfo* etudiants[], int nbEtudiants, char* ville, char* dep) {
  DepartementVille* departementVille = malloc(sizeof(DepartementVille));
  departementVille->nbEtudiants = 0;
  strcpy(departementVille->ville, ville);
  strcpy(departementVille->departement, dep);

  departementVille->etudiants = malloc(nbEtudiants * sizeof(listetuinfo));

  for (int i = 0; i < nbEtudiants; i++) {
    // si l'étudiant a fait le choix de ce département dans cette ville, l'ajouter au tableau de la structure
    if (strcmp(etudiants[i]->carte->Ville, ville) == 0 && strcmp(etudiants[i]->carte->dep, dep) == 0) {
      etudiants[i]->moyenne = calculerMoyenne(*etudiants[i]);

      // ajouter l'étudiant au tableau de la structure
      departementVille->etudiants[departementVille->nbEtudiants] = etudiants[i];
      departementVille->nbEtudiants++;
    }
  }

  return departementVille;
}

float calculerMoyenne(listetuinfo etudiant) {
  float somme = 0.0;
  for (int i = 0; i < 4; i++) {
    somme += etudiant.notes[i];
  }
  return somme / 4.0;
}

void trierDepartementVille(DepartementVille* departementVille, int nbElements) {
	for (int i = 0; i < nbElements; i++) {
		for (int j = i + 1; j < nbElements; j++) {
			if (departementVille->etudiants[j]->moyenne > departementVille->etudiants[i]->moyenne) {
				listetuinfo* etudiantTemp = departementVille->etudiants[i];
				departementVille->etudiants[i] = departementVille->etudiants[j];
				departementVille->etudiants[j] = etudiantTemp;
			}
		}
	}
}

void comparerEtudiants(DepartementVille* departementVille, listetuinfo* etudiants, int nbPlaces) {
	for (int i = 0; i < departementVille->nbEtudiants; i++) {
		// si l'étudiant a une moyenne inférieure à 10, mettre sa variable "decision" à -1
		if (departementVille->etudiants[i]->moyenne < 10) departementVille->etudiants[i]->carte->decision = -1;
		// sinon, si l'index de l'étudiant est inférieur ou égal au nombre de places disponibles, mettre sa variable "decision" à 1
		else if (i < nbPlaces) departementVille->etudiants[i]->carte->decision = 1;
		// sinon, mettre sa variable "decision" à 2
		else departementVille->etudiants[i]->carte->decision = 2;
	}
}

DepartementVille* useAllFunctionsCandidature(listetuinfo* tetu[], int nbEtu) {
	DepartementVille* departementVille;
	departementVille = recupererEtudiantsDepartementVille(tetu, nbEtu, "Clermont-Ferrand", "informatique");
	trierDepartementVille(departementVille, departementVille->nbEtudiants);
	comparerEtudiants(departementVille, *tetu, 5);
	return departementVille;
}

void afficherDepartementVille(DepartementVille* departementVille) {
  printf("Ville : %s\n", departementVille->ville);
  printf("Departement : %s\n", departementVille->departement);
  printf("Nombre d'étudiants : %d\n", departementVille->nbEtudiants);
  printf("Etudiants :\n");
  for (int i = 0; i < departementVille->nbEtudiants; i++) {
    printf("\t- %s %s (numero d'étudiant : %d, moyenne : %.2f", departementVille->etudiants[i]->nometu, departementVille->etudiants[i]->prenometu, departementVille->etudiants[i]->numeroetu, departementVille->etudiants[i]->moyenne);
	if (departementVille->etudiants[i]->carte->decision == 1) printf("decision : accepté (1))\n");
	if (departementVille->etudiants[i]->carte->decision == 2) printf("decision : en attente (2))\n");
  }
}



// Partie 4
/*
	V affichage coté candidat de ces infos admis ou pas
	V valdier ça candidature refuse les autres
	- valider met a jour le dossier candidature
	- chaque departement peut consulter ça liste d'attente
	- metre a jour la liste 
	

*/

// coté etu=========================================================================================================
// affichage

void affichagedepresultat(Maillonchoix* carte, int *suite){
	if(carte==NULL){return;}
	if(carte->decision==1){
		printf("%s %s - admis \n", carte->Ville, carte->dep);
		*suite=1;
	}
	if(carte->decision==-1){
		printf("%s %s - refusé \n", carte->Ville, carte->dep);
	}
	if(carte->decision==2){
		printf("%s %s - lsite d'attente \n", carte->Ville, carte->dep);
	}
	affichagedepresultat(carte->suivant, suite);
}

// validation 

void validation(char choixville[], char choixdep[], Maillonchoix* carte, int *ok){
	if(carte==NULL){return;}
	if(strcmp(carte->Ville,choixville)==0){
		if(strcmp(carte->dep,choixdep)==0){
			if(carte->decision==1){
				carte->validation=1;
				*ok=1;
			}else{printf("vous netes pas admis dans cette iut\n");*ok=-1;return;}
		}
	}
	validation(choixville, choixdep, carte->suivant, ok);
	if(*ok==1){
		if(carte->decision==1);
	}
}

void afficheroneetu(listetuinfo *tetu[], char choixnom[], char choixprenom[], int nbEtu){
	int i;
	listetuinfo etu;
	for(i=0; i<nbEtu; i++){
		if(strcmp(tetu[i]->nometu,choixnom)==0){
			if(strcmp(tetu[i]->prenometu,choixprenom)==0){
				etu = *tetu[i];
				affichageetu(etu);
			}
		}
	}
}

// trier pour afficher par ordre alphabétique=============================================================

void Permute (listetuinfo *tetutri[], int r1, int r2){
	listetuinfo *temp;
	temp=tetutri[r1];
	tetutri[r1]=tetutri[r2];
	tetutri[r2]=temp;
}

int recherchemin(listetuinfo *tetutri[], int nbEtu, int i){
	int j,r;
	r=i;
	for(j=i+1; j<nbEtu; j++){
		if(strcmp(tetutri[j]->nometu,tetutri[r]->nometu)<0){
			r=j;
			if(strcmp(tetutri[j]->prenometu,tetutri[r]->prenometu)<0){
				r=j;
			}
		}
	}
	return r;
}

void triertetu (listetuinfo *tetutri[], int nbEtu){
	int i,r;
	for(i=0;i<nbEtu; i++){
		r=recherchemin(tetutri, nbEtu, i);
		Permute(tetutri, r, i);
	}
}


// MENU PARTIE 1

void menuValidation(listetuinfo etu){
	int ok;
	char choixville[31], choixdep[31];
	Listchoixdept lcarte;
	lcarte=etu.carte;
	choix2(lcarte, choixville, choixdep);
	validation(choixville, choixdep, lcarte, &ok);
}

listetuinfo menuP1Utilisateur(listetuinfo etu, VilleIUT *tabVille[], int nb, listetuinfo *tetu[], int nbEtu, DepartementVille *departementVille, char nomFich[]) {
	int choixP1U;
	int suite=0;
	int reponse;
	Maillonchoix* lcarte;
	while (choixP1U >= 1 || choixP1U <= 4) {
		printf("\n");
		printf("---------------------- MENU UTILISATEUR -----------------------\n");
		printf("|                                                             |\n");
		printf("|   1 - Consulter les villes avec un IUT                      |\n");
		printf("|   2 - Consulter les départements d'un IUT                   |\n");
		printf("|   3 - Consulter le nombre de place des départements         |\n");
		printf("|   4 - Consulter les IUT possédant un certain département    |\n");
		printf("|   5 - Ajouter un souhait                                    |\n");
		printf("|   6 - Modifer un souhait                                    |\n");
		printf("|   7 - Supprimer un souhait                                  |\n");
		printf("|   8 - Consulter ses informations                            |\n");
		printf("|   9 -	Consulter où l'on est admis                           |\n");
		printf("|                                                             |\n");
		printf("|   15 - Retour                                               |\n");
		printf("|   Votre choix => ");
		scanf("%d", &choixP1U);
		printf("---------------------------------------------------------------\n");
		printf("\n");
		switch (choixP1U) {
			case 1:
				afficheVilleIUT(tabVille, nb);
				break;
			case 2:
				afficheDepartementsIUT(tabVille, nb);
				break;
			case 3:
				affichageville(tabVille, nb);
				break;
			case 4:
				afficheIUTdepParticulier(tabVille, nb);
				break;
			case 5:
				etu=allajoutprocess(tabVille, etu, nb);
				etu.nbchoix = etu.nbchoix+1;
				break;
			case 6:
				modificationcarte(etu, tabVille, nb);
				break;
			case 7:
				suppcarte(etu);
				etu.nbchoix = etu.nbchoix-1;
				break;
			case 8:
				affichageetu(etu);
				lcarte=etu.carte;
				affichagecarte(lcarte);
				break;
			case 9:
				lcarte=etu.carte;
				affichagedepresultat(lcarte, &suite);
				if(suite==1){
					printf("|   1 - valider un veux               	  		       		  |\n");
					printf("|   autre chiffre pour sortir     					          |\n");
					printf("|	=>");
					scanf("%d", &reponse);
					//le menu de validation avec choix pour ce que l'on peux valider et apres -> validation et enregistrement
					if(reponse==1){
						menuValidation(etu);
						enregistrementetudiants(nomFich, tetu, nbEtu);
						departementVille = useAllFunctionsCandidature(tetu, nbEtu);
					}
				}
				break;
			case 15:
				return etu;
		}
	}
}

void menuP1Administrateur(VilleIUT *tabVille[], int nb, listetuinfo *tetu[], int nbEtu, DepartementVille *departementVille, listetuinfo *tetutri[]) {
	int choixP1A = 0;
	char choixnom[21], choixprenom[21];
	while (choixP1A >= 1 || choixP1A <= 4) {
		printf("\n");
		printf("---------------------- MENU ADMINISTRATEUR -----------------------\n");
		printf("|                                                                |\n");
		printf("|   1 - Modifier le nombre de place d'un département             |\n");
		printf("|   2 - Créer un département                                     |\n");
		printf("|   3 - Créer un IUT                                             |\n");
		printf("|   4 - Supprimer un département                                 |\n");
		printf("|   5 - Modifier nom responsable                                 |\n");
		printf("|   6 - Lancer analyse candidature                               |\n");
		printf("|   7 - Afficher liste DES candidatures                          |\n");
		printf("|   8 - Afficher une seul etudiant                               |\n");
		printf("|   9 - Afficher candidats par odre alphabétique                 |\n");
		printf("|   10 - Afficher les résultats candidature                      |\n");
		printf("|                                                                |\n");
		printf("|   15 - Retour                                                  |\n");
		printf("|   Votre choix => ");
		scanf("%d", &choixP1A);
		printf("------------------------------------------------------------------\n");
		printf("\n");
		switch (choixP1A) {
			case 1:
				modifPlace(tabVille, nb);
				break;
			case 2:
				ajoutDept(-1, tabVille, nb);
				break;
			case 3:
				ajoutIUT(tabVille, &nb, 100);
				break;
			case 4:
				supprDepartement(tabVille, &nb);
				break;
			case 5:
				modifNomResponsable(tabVille, nb);
				break;
			case 6:
				departementVille = useAllFunctionsCandidature(tetu, nbEtu);
				printf("L'analyse et le classement a bien été effectué !\n");
				break;
			case 7:
				affichealletu(tetu, nbEtu);
				break;
			case 8:
				printf("|   quel est le nom recherché ?                                  |\n");
				printf("|	=>");
				fgets(choixnom, 21, stdin);
				fgets(choixnom, 21, stdin);
				choixnom[strlen(choixnom)-1]='\0';
				printf("|   quel est le prénom recherché ?                               |\n");
				printf("|	=>");
				fgets(choixprenom, 21, stdin);
				choixprenom[strlen(choixprenom)-1]='\0';
				afficheroneetu(tetu, choixnom, choixprenom, nbEtu);
				break;
			case 9:
				triertetu(tetutri, nbEtu);
				affichealletu(tetutri, nbEtu);
				break;
			case 10:
				afficherDepartementVille(departementVille);
				break;
			case 15:
				sauvegarde(tabVille, nb);// mettre menu général
				return;
		}
	}
}

void menuPrincipal(VilleIUT *tabVille[], int nb, listetuinfo *tetu[], int nbEtu, DepartementVille* departementVille, char nomFich[], listetuinfo *tetutri[]) {
	int choix = 0, ligneetu, num;
	listetuinfo etu;
	while (choix >= 1 || choix <= 4) {
		printf("\n");
		printf("---------------------- MENU PRINCIPAL -----------------------\n");
		printf("|                                                           |\n");
		printf("|   1 - Utilisateur                                         |\n");
		printf("|   2 - Administrateur                                      |\n");
		printf("|                                                           |\n");
		printf("|   9 - Quitter                                             |\n");
		printf("|                                                           |\n");
		printf("|   Votre choix => ");
		scanf("%d", &choix);
		printf("-------------------------------------------------------------\n");
		printf("\n");
		switch (choix) {
			case 1:
				printf("|   - quel est votre numéro de candidat ?\n|   =>");
				scanf("%d", &num);
				ligneetu = recherdicoetu(tetu, nbEtu, num);
				etu = *tetu[ligneetu];
				etu =menuP1Utilisateur(etu, tabVille, nb, tetu, nbEtu, departementVille, nomFich);
				*tetu[ligneetu] = etu;
				break;
			case 2:
				menuP1Administrateur(tabVille, nb, tetu, nbEtu, departementVille, tetutri);
				break;
			case 9:
				sauvegarde(tabVille, nb);
				//sauvegardeTabRespFichier(tabVille, nb, tabResp);
				enregistrementetudiants(nomFich, tetu, nbEtu);
				return;
		}
	}
}

void copier(listetuinfo *tetu[], listetuinfo *tetutri[], int nbEtu){
	int i;
	for(i=0; i<nbEtu; i++){
		tetutri[i]=tetu[i];
	}
}

void test(void){
	#define TAILLEMAX 200
	VilleIUT *tabVille[TAILLEMAX];
	int nb = chargeIUT(tabVille, TAILLEMAX);
	
	listetuinfo *tetu[TAILLEMAX];
	listetuinfo *tetutri[TAILLEMAX];
	int nbEtu;
	char nomFich[20];
	strcpy( nomFich, "candidature.txt");
	nbEtu = chargeretudiant(nomFich, tetu, 5000);
	copier(tetu, tetutri, nbEtu);

	DepartementVille* departementVille;
	menuPrincipal(tabVille, nb, tetu, nbEtu, departementVille, nomFich, tetutri);
}
