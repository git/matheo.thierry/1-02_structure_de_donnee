/*
	file: sae1-02.h 
	author: mathéo THIERRY
			flavien BOUHAMDANI
	date: 13 dec 2022
	permet: 
*/

/*
	brief:
*/


// Partie 1
typedef struct listville {
	char departement[31];
	int nbplace;
	char responsable[31];
	struct listville* suivant;
} MaillonDept, *Listdept;

typedef struct {
	char Ville[31];
	int nbIUT;
	Listdept ldept;
} VilleIUT;

//chargement
Listdept initialisation(void);
VilleIUT lireVille(FILE *fe);
Listdept lireDept(FILE *fe, int nbIUT);
int chargeIUT(VilleIUT* tIUT[], int taillemax);

//affichage
void affichageDep(Listdept d);
void affichageAllDep(Listdept d);
void affichageville(VilleIUT *tville[], int nbville);
void afficheVilleIUT(VilleIUT *tville[], int nbville);
void afficheDepartementsIUT(VilleIUT *tville[], int nbville);
void afficheDepNbPlace(VilleIUT *tville[], int nbville);
void afficheIUTdepParticulier(VilleIUT *tville[], int nbville);

//modif
int rechercheVille(VilleIUT *tville[], int nbville);
void modifPlace(VilleIUT *tville[], int nbville);
int chercheDepModif(Listdept d, char rechercheDep[31]);
void modifNomResponsable(VilleIUT *tville[], int nbville);

//ajout / suppressions
Listdept ajoutDept(int posOptionnel, VilleIUT *tville[], int nbville);
void ajoutIUT(VilleIUT *tIUT[], int *nbville, int taillemax);
Listdept suppressionDept(Listdept d, char rechercheDep[31]);
void supprDepartement(VilleIUT *tville[], int* nbville);

void sauvegardeDep(Listdept d, FILE *fs);
void sauvegarde(VilleIUT *tville[], int nbville);

// Partie 2

typedef struct listchoix{
	char Ville[31];
	char dep[31];
	int decision;
	int validation;
	struct listchoix* suivant;
}Maillonchoix, *Listchoixdept;

typedef struct{
	int numeroetu;
	char nometu[21];
	char prenometu[21];
	int notes[4];
	int nbchoix;
	float moyenne; // partie 3
	Listchoixdept carte;
}listetuinfo;

Listchoixdept listvide(void);
Listchoixdept lireCarte(FILE *fe, int nbchoix);
listetuinfo lireEtu(FILE *fe);
int chargeretudiant(char nomFich[], listetuinfo *tetu[], int tmax);
int recherdicoetu(listetuinfo *tetu[], int nb, int num);
void enregistrementcarte(FILE *fe, Maillonchoix* carte, int nb);
void enregistrementinfo(FILE *fe, listetuinfo *etu);
int enregistrementetudiants(char nomFich[], listetuinfo *tetu[], int nb);
void affichagecarte(Listchoixdept c);
void affichageetu(listetuinfo a);
void affichealletu(listetuinfo *tetu[], int nb);
int cherchelistcorrespond(MaillonDept* dep, char choixdep[]);
int correspondville(char choixville[], char choixdep[], VilleIUT *tville[], int nbville);
int correspondcarte(char choixville[], char choixdep[], Listchoixdept carte);
void choix(VilleIUT *tville[], int nbville, char choixville[], char choixdep[]);
Listchoixdept ajoutercarte(Maillonchoix* a, char choixville[], char choixdep[]);
listetuinfo allajoutprocess(VilleIUT *tville[], listetuinfo etu, int nbville);
void choix2(Listchoixdept lcarte, char choixville[], char choixdep[]);
void fchoixdep(char choixville[], char choixdep[], VilleIUT *tville[], int nbville);
Listchoixdept choixmodifcarte(Listchoixdept dep, char choixville[], char choixdep[], VilleIUT *tville[], int nbville);
void affichagedep(MaillonDept* carte);
void affichervillechoix(VilleIUT *villechoix);
int correspondvilleonly(char choixville[], VilleIUT *ville[], int nbville);
void modificationcarte(listetuinfo etu, VilleIUT *tville[], int nbville);
Maillonchoix* suppressioncartechoix(char choixville[], char choixdep[], Listchoixdept carte);
void suppcarte(listetuinfo etu);





// Partie 3

typedef struct {
  char ville[31];
  char departement[31];
  listetuinfo** etudiants;
  int nbEtudiants;
} DepartementVille;

DepartementVille* recupererEtudiantsDepartementVille(listetuinfo* etudiants[], int nbEtudiants, char* ville, char* dep);
float calculerMoyenne(listetuinfo etudiant);
void trierDepartementVille(DepartementVille* departementVille, int nbElements);
void comparerEtudiants(DepartementVille* departementVille, listetuinfo* etudiants, int nbPlaces);
void afficherDepartementVille(DepartementVille* departementVille);
DepartementVille* useAllFunctionsCandidature(listetuinfo* tetu[], int nbEtu);


// Partie 4
typedef struct listatt{
	int numc;
	char nom[21];
	char prenom[21];
	int decision;
	struct listatt* suivant;
}Maillonatt;

void test(void);

listetuinfo menuP1Utilisateur(listetuinfo etu, VilleIUT *tabVille[], int nb, listetuinfo *tetu[], int nbEtu, DepartementVille *departementVille, char nomFich[]);
void menuP1Administrateur(VilleIUT *tabVille[], int nb, listetuinfo *tetu[], int nbEtu, DepartementVille* departementVille, listetuinfo *tetutri[]);
void menuPrincipal(VilleIUT *tabVille[], int nb, listetuinfo *tetu[], int nbEtu, DepartementVille* departementVille, char nomFich[], listetuinfo *tetutri[]);